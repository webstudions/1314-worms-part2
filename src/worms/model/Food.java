package worms.model;

import be.kuleuven.cs.som.annotate.*;

public class Food extends GameObject {
	
	/**
	 * Create a new food ration that is positioned at the given location in the given world.
	 * 
	 * @param world
	 * The world in which to place the created food ration
	 * @param x
	 * The x-coordinate of the position of the new food ration (in meter)
	 * @param y
	 * The y-coordinate of the position of the new food ration (in meter)
	 * 
	 * (For single-student groups that do not implement food, this method should have no effect)
	 */
	public Food(World world, double x, double y) {
		super(x, y, radius);
	}
	
	/**
	 * Returns the radius of the given food ration
	 */
	@Basic @Immutable
	public double getRadius() {
		return Food.radius;
	};
	
	private final static double radius = 0.2;

}
