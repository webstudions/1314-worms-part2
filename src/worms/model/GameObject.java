package worms.model;

import be.kuleuven.cs.som.annotate.Basic;

public abstract class GameObject {

	protected GameObject(double x, double y, double radius) {
		try {
			this.position = new Position(x,y);
		} catch(IllegalArgumentException exc) {
			throw new IllegalArgumentException();
		}
		this.setRadius(radius);
	}
	
	/**
	 * Return the radius of this GameObject, in meters.
	 */
	@Basic
	public double getRadius() {
		return radius;
	}
	
	/**
	 * Sets the radius of this GameObject to the given radius.
	 * @post	Sets the radius of this GameObject to the given radius.
	 * 			| this.radius = radius
	 * @param radius
	 */
	private void setRadius(double radius) {
		this.radius = radius;
	}

	private double radius;
	
	public void setX(double x) {
		this.position = new Position(x, this.getY());
	}
	
	/**
	 * Returns the x-coordinate of the given food ration
	 */
	public double getX() {
		return this.position.getX();
	};
	
	public void setY(double y) {
		this.position = new Position(this.getY(), y);
	}
	
	/**
	 * Returns the y-coordinate of the given food ration
	 */
	public double getY() {
		return this.position.getY();
	};
	
	private Position position;
	
	public boolean isOutsideWorld() {
		return ( (this.getX() > this.getWorld().getWidth()) || 
				 (this.getX() < 0) || 
				 (this.getY() > this.getWorld().getHeight()) ||
				 (this.getY() < 0));
	}
	
	/**
	 * Returns whether or not the given food ration is alive (active), i.e., not eaten.
	 * 
	 * (For single-student groups that do not implement food, this method should always return false)
	 */
	public boolean isActive() {
		return this.active;
	};
	
	private boolean active;
}
