package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import worms.util.Util;

public abstract class Jumpable extends GameObject {
	
	public Jumpable(double x, double y, double radius) {
		super(x, y, radius);
	}
	
	/**
	 * Return the mass of this jumpable object, in kilograms.
	 */
	@Basic
	public double getMass() {
		return this.mass;
	}
	
	protected abstract void setMass();
	
	protected double mass;
	
	/**
	 * Return the direction this jumpable object is facing, in radians.
	 */
	@Basic
	public double getDirection() {
		return direction;
	}
	
	/**
	 * Sets the direction of this jumpable object to the given direction.
	 * @pre		The given direction is a number that lies between 0 and 2 PI.
	 * 			| isValidDirection( direction )
	 * @post	Sets the direction of this jumpable object to the given direction.
	 * 			| new.getDirection() == direction
	 * @param 	direction
	 * 			The direction this jumpable object is going to facing in radians.
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(direction)
	 */
	@Raw
	public void setDirection(double direction) {
		assert isValidDirection(direction);
		this.direction = direction;
	}
	
	/**
	 * Check if the given direction lies between 0 and 2PI.
	 * @param direction
	 * @return	Return true if the given direction lies between 0 (included) and 2 PI (included).
	 * 			| result == ( (0 <= direction) && (direction <= 2*MATH.PI) && (! Double.isNaN(direction) )
	 */
	public boolean isValidDirection(double direction) {
		return ( Util.fuzzyLessThanOrEqualTo(0, direction) ) && ( Util.fuzzyLessThanOrEqualTo(direction, 2*Math.PI) && (! Double.isNaN(direction)) );
	}
	
	private double direction;
	
	/**
	 * 
	 * @param timeStep
	 */
	public abstract void jump(double timeStep);
	
	/**
	 * Makes the object jump.
	 * @post	The x value of the object is increased by the x distance of the jump.
	 * 			| new.getX() == this.getX() + distanceX
	 * @post	The y value of the object is increased by the y distance of the jump.
	 * 			| new.getY() == this.getY() + distanceY
	 * @throws 	UnsupportedOperationException
	 * 			If this worm wants to jump while facing down.
	 * 			| (Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)
	 */
	public void jump(double timeStep, double force) throws UnsupportedOperationException {
		if(Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)) 
			throw new UnsupportedOperationException();
		double v0 = (force/this.getMass()) * 0.5;
		double distanceX;
		double distanceY;
		if(Util.fuzzyEquals(this.getDirection(),Math.PI/2)) {
			distanceX = 0.0;
			distanceY = 0.0;
		} else {
			distanceX = v0 * Math.cos(this.getDirection()) * this.getJumpTime();
			distanceY = v0 * Math.sin(this.getDirection()) * this.getJumpTime() - 0.5 * gravity * Math.pow(this.getJumpTime(), 2);
		}
		this.setX( this.getX() + distanceX );
		this.setY( this.getY() + distanceY );
	}
	
	/**
	 * Returns the location on the jump trajectory of the given projectile after a
	 * time t.
	 * 
	 * @return An array with two elements, with the first element being the
	 *         x-coordinate and the second element the y-coordinate
	 */
	
	/**
	 * Returns the total amount of time (in seconds) that a jump of the given worm would take.
	 * @return	Return the total amount of time (in seconds) that a jump of the given worm would take.
	 * 			| result == distance/(v0 * Math.cos(this.getDirection()))
	 * @throws	ArithmeticException
	 * 			If this worm is trying to jump while facing straight up.
	 * 			| if( Util.fuzzyEquals(this.getDirection(), Math.PI/2) )
	 * @throws	UnsupportedOperationException
	 * 			If this worm doesn't have any action points anymore.
	 * 			| if( this.getActionPoints() == 0 )
	 * @throws 	UnsupportedOperationException
	 * 			If this worm wants to jump while facing down.
	 * 			| (Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)
	 */
	public double getJumpTime() throws ArithmeticException, UnsupportedOperationException {
		if( Util.fuzzyEquals(this.getDirection(), Math.PI/2) ) 
			throw new ArithmeticException();
		if(this.getActionPoints() == 0 )
			throw new UnsupportedOperationException();
		if(Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)) 
			throw new UnsupportedOperationException();
		double force = (5 * this.getActionPoints()) + this.getMass() * gravity;
		double v0 = (force/this.getMass()) * 0.5;
		double distance = (Math.pow(v0, 2) * Math.sin(2 * this.getDirection()) / gravity);
		return distance/(v0 * Math.cos(this.getDirection()));
	};

	/**
	 * Returns the location on the jump trajectory of the given worm
	 * after a time t.
	 * @param t
	 * @return 	An array with two elements, with the first element being the x-coordinate and the second element the y-coordinate.
	 * 			| double x = this.getX() + v0 * Math.cos(this.getDirection()) * t;
	 * 			| double y = this.getY() + v0 * Math.sin(this.getDirection()) * t - 0.5 * gravity * Math.pow(t,2);
	 * 			| result == {x,y}
	 * @throws	IllegalArgumentException
	 *  		| Double.isNaN(t)
	 * @throws	IllegalArgumentException
	 * 			| (t < 0) || (t > this.getJumpTime)
	 */
	public double[] getJumpStep(double t) throws IllegalArgumentException {
		if(Double.isNaN(t)) 
			throw new IllegalArgumentException();
		if((t < 0) || (t > this.getJumpTime())) 
			throw new IllegalArgumentException();
		double force = (5 * this.getActionPoints()) + this.getMass() * gravity;
		double v0 = (force/this.getMass()) * 0.5;
		double x = this.getX() + v0 * Math.cos(this.getDirection()) * t;
		double y = this.getY() + v0 * Math.sin(this.getDirection()) * t - 0.5 * gravity * Math.pow(t,2);
		double[] result = {x,y}; 
		return result;	
	};
	
	protected final double gravity = 9.80665;

}
