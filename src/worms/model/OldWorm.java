package worms.model;

import be.kuleuven.cs.som.annotate.*;
import worms.util.Util;

/**
 * @invar	The radius of this worm is at all times bigger than the minimum radius required.
 * 			| isValidRadius(this.getRadius());
 * @invar	The action points of this worm are at all times bigger than 0 (or equal to) and smaller than (or equal to) the maximum action points.
 * 			| (this.getActionPoints() >= 0) && (this.getActionPoints <= this.getMaximumActionPoints())
 * @invar	The name of this worm at all times matches the regular expression: "[A-Z]{1}[a-zA-Z\" \"[\"][\']]+"
 * 			| isValidName(this.getName())
 * @invar	The direction of this worm at all times lies between 0 and 2 PI
 * 			| isValidDirection( this.getDirection() )
 * @author nickscheynen
 * @author carmenvandeloo
 *
 */
public class OldWorm {
	
	/**
	 * Declaration of all describing variables of a worm.
	 */
	
	/**
	 * Initializing this worm with the given position (x and y), direction, radius and name.
	 * @param 	x
	 * 		  	The x coordinate for this worm in meters.
	 * @param 	y
	 * 		  	The y coordinate for this worm in meters.
	 * @param 	direction
	 * 		  	The direction this worm is facing in radians.
	 * @param 	radius
	 * 		  	The radius of this worm in meters.
	 * @param 	name
	 * 		  	The name of this worm.
	 * @effect	The x coordinate of this worm is set using the setX() method.
	 * 			| this.setX(x)
	 * @effect	The y coordinate of this worm is set using the setY() method.
	 * 			| this.setY(y)
	 * @effect	The direction of this worm is set using the setDirection() method.
	 * 			| this.setDirection(direction)
	 * @effect	Sets the radius, mass and maximum action points using the setSizeAspects() method.
	 * 			| this.setSizeAspects(radius)
	 * @effect	The name of this worm is set using the setName() method.
	 * 			| this.setName(name)
	 * @effect	The number of action points of this worm is set to the max AP using the setActionPoints() method.
	 * 			| this.setActionPoints(this.getMaximumActionPoints())
	 * 
	 */
	@Raw
	public OldWorm(double x, double y, double direction, double radius, String name) {
		this.setX(x);
		this.setY(y);
		this.setDirection(direction);
		this.setSizeAspects(radius);
		this.setName(name);
		this.setActionPoints(this.getMaximumActionPoints());
	}
	
	/*
	 * Getters and Setters for the describing variables of a worm.
	 */
	
	/**
	 * Return the x coordinate of this worm, in meters.
	 */
	@Basic 
	public double getX() {
		return x;
	}
	
	/**
	 * Sets the x coordinate of this worm to the given value.
	 * @post	Sets the x coordinate of this worm to the given value.
	 * 			| new.getX() == x
	 * @param 	x	
	 * 			The x coordinate of this worm in meters. 
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(x)
	 */
	@Raw
	public void setX(double x) throws IllegalArgumentException {
		if(Double.isNaN(x))
			throw new IllegalArgumentException();
		this.x = x;
	}
	
	private double x;
	
	/**
	 * Return the y coordinate of this worm, in meters.
	 */
	@Basic
	public double getY() {
		return y;
	}
	
	/**
	 * Sets the y coordinate of this worm to the given value.
	 * @post	Sets the y coordinate of this worm to the given value.
	 * 			| new.getY() == y
	 * @param 	y
	 * 			The y coordinate of this worm in meters.
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(y)
	 */
	@Raw
	public void setY(double y) {
		if(Double.isNaN(y)) 
			throw new IllegalArgumentException();
		this.y = y;
	}
	
	private double y;
	
	/**
	 * Return the direction this worm is facing, in radians.
	 */
	@Basic
	public double getDirection() {
		return direction;
	}
	
	/**
	 * Sets the direction of this worm to the given direction.
	 * @pre		The given direction is a number that lies between 0 and 2 PI.
	 * 			| isValidDirection( direction )
	 * @post	Sets the direction of this worm to the given direction.
	 * 			| new.getDirection() == direction
	 * @param 	direction
	 * 			The direction this worm is facing in radians.
	 * @throws	IllegalArgumentException
	 * 			| Double.isNaN(direction)
	 */
	@Raw
	public void setDirection(double direction) {
		assert isValidDirection(direction);
		this.direction = direction;
	}
	
	/**
	 * Check if the given direction lies between 0 and 2PI.
	 * @param direction
	 * @return	Return true if the given direction lies between 0 (included) and 2 PI (included).
	 * 			| result == ( (0 <= direction) && (direction <= 2*MATH.PI) && (! Double.isNaN(direction) )
	 */
	public static boolean isValidDirection(double direction) {
		return ( Util.fuzzyLessThanOrEqualTo(0, direction) ) && ( Util.fuzzyLessThanOrEqualTo(direction, 2*Math.PI) && (! Double.isNaN(direction)) );
	}
	
	private double direction;
	
	/**
	 * Sets the radius of this worm to the given radius.
	 * @effect	Updates the radius of this worm, using the setRadius() method.
	 * 			| this.setRadius(radius)
	 * @effect	Updates the mass of this worm, using the setMass() method.
	 * 			| this.setMass()
	 * @effect	Updates the maximum action points, using the setMaximumActionPoints() method.
	 * 			| this.setMaximumActionPoints()
	 * @effect 	Limits the action points using the setActionPoints() method if the action points exceed their maximum.
	 * 			| if(this.getActionPoints()>this.getMaximumActionPoints()) this.setActionPoints(this.getMaximumActionPoints());
	 * @param 	radius
	 * 			The radius of this worm in meters.
	 * @throws 	IllegalArgumentException
	 * 			| ! isValidRadius(radius) || Double.isNaN(radius)
	 */
	@Raw
	public void setSizeAspects(double radius) throws IllegalArgumentException
	{
		if( ( !isValidRadius(radius)) ) 
			throw new IllegalArgumentException();
		this.setRadius(radius);
		this.setMass();
		this.setMaximumActionPoints();
		if(this.getActionPoints()>this.getMaximumActionPoints()) {
			this.setActionPoints(this.getMaximumActionPoints());
		}
	}
	
	/**
	 * Return the radius of this worm, in meters.
	 */
	@Basic
	public double getRadius() {
		return radius;
	}

	/**
	 * Sets the radius of this worm to the given radius.
	 * @post	Sets the radius of this worm to the given radius.
	 * 			| this.radius = radius
	 * @param radius
	 */
	private void setRadius(double radius) {
		this.radius = radius;
	}
	
	/**
	 * Check if the radius is bigger than or equal to the minimum radius.
	 * @param radius
	 * @return	Return true if the radius is bigger than or equal to the minimum radius.
	 * 			| result == Util.fuzzyGreaterThanOrEqualTo(radius, minimumRadius) && !Double.isNaN(radius)
	 */
	public static boolean isValidRadius(double radius) {
		return Util.fuzzyGreaterThanOrEqualTo(radius, this.getMinimumRadius()) && !Double.isNaN(radius);
	}
	
	private double radius;
	
	/**
	 * Return the name of this worm.
	 */
	@Basic
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of this worm to the given name.
	 * @post	Sets the name of this worm to the given name.
	 * 			| new.getName() == name
	 * @param name
	 * @throws	IllegalArgumentException
	 * 			| ! isValidName(name)
	 */
	@Raw
	public void setName(String name) throws IllegalArgumentException {
		if(! isValidName(name))
			throw new IllegalArgumentException();
			this.name = name;
	}
	
	/**
	 * Check if the name is valid.
	 * @param name
	 * @return	Return true if the name matches the regular expression. 
	 * 			Names must start with a capital letter, be at least 2 characters long and 
	 * 			can only use letters (uppercase and lower case), quotes (single and double) and spaces.
	 * 			| result == name.matches("[A-Z]{1}[a-zA-Z\" \"[\"][\']]+");
	 */
	public static boolean isValidName(String name) {
		return name.matches("[A-Z]{1}[a-zA-Z\" \"[\"][\']]+");
	}
	
	private String name;
	
	/**
	 * Return the mass of this worm, in kilograms.
	 */
	@Basic
	public double getMass() {
		return this.mass;
	}
	
	/**
	 * Sets the mass of this worm to the given mass.
	 * @post	Sets the mass of this worm using the radius of this worm, expressed in kilograms.
	 * 			| new.getMass() == density * ( 4/3 * Math.PI * Math.pow(this.getRadius(), 3) );
	 */
	@Raw
	private void setMass() {
		this.mass = density * ( (4.0/3.0) * Math.PI * Math.pow(this.getRadius(), 3) );
	}
	
	private double mass;
	private final double density = 1062;
	
	/**
	 * Return the maximum action points this worm can have.
	 */
	@Basic
	public int getMaximumActionPoints() {
		return this.maximumActionPoints;
	}
	
	/**
	 * Sets the maximum action points this worm can have based on its mass.
	 * @post	Sets the maximum action points of this worm using its mass. If the mass leads to a too large integer, max action points are set to max integer.
	 * 			| long newMAP = Math.round( this.getMass() );
				| if (newMAP >= Integer.MAX_VALUE) {
				| 	this.maximumActionPoints = Integer.MAX_VALUE;
				| } else {
				|	this.maximumActionPoints = (int) newMAP; 
				| }
	 */
	@Raw
	private void setMaximumActionPoints() {
		long newMAP = Math.round( this.getMass() );
		if (newMAP >= Integer.MAX_VALUE) {
			this.maximumActionPoints = Integer.MAX_VALUE;
		} else {
			this.maximumActionPoints = (int) newMAP; 
		}
	}

	private int maximumActionPoints;
	
	/**
	 * Return the number of action points of this worm.
	 */
	@Basic
	public int getActionPoints() {
		return actionPoints;
	}
	
	/**
	 * Set the action points of this worm to the given number of action points.
	 * @post	Set the action points of this worm to the given number of action points.
	 * 			| if(actionPoints < 0) { 
	 * 			|		new.getActionPoints() == actionPoints;
	 * 			|	}
	 * 			| else if( (actionPoints >= 0) && (actionPoints<=this.getMaximumActionPoints() ) 
	 * 			|	{
	 * 			|		this.actionPoints = actionPoints;
	 * 			|	}
	 * 			| else {
	 * 			|		this.actionPoints = this.getMaximumActionPoints();	
	 * 			|	}
	 * @param actionPoints
	 */
	@Raw
	public void setActionPoints(int actionPoints) {
		if(actionPoints < 0) {
			this.actionPoints = 0;
		} else if( (actionPoints>=0) && (actionPoints<=this.getMaximumActionPoints()) ) {
			this.actionPoints = actionPoints;
		} else { // bigger than the maximum
			this.actionPoints = this.getMaximumActionPoints();
		}
	}
	
	private int actionPoints;
	
	/**
	 * Return the minimum allowed radius for this worm.
	 */
	@Basic @Immutable
	public double getMinimumRadius() {
		return this.minimumRadius;
	}
	
	private final double minimumRadius = 0.25;
	
	
	/*
	 * Action Methods (turning, moving, jumping)
	 */
	
	/**
	 * Returns whether or not the given worm can move a given number of steps.
	 * @param nbSteps
	 * @return	Returns whether or not the given worm can move a given number of steps.
	 * 			| result == this.calculateMoveCost(nbSteps) <= this.getActionPoints();
	 * @throws IllegalArgumentException
	 * 			| nbSteps < 0
	 */
	public boolean canMove(int nbSteps) throws IllegalArgumentException {
		if(nbSteps < 0) 
			throw new IllegalArgumentException();
		return this.calculateMoveCost(nbSteps) <= this.getActionPoints();
	};
	
	/**
	 * Moves the given worm by the given number of steps in the current direction.
	 * @post	Moves the given worm by the given number of steps in the current direction.
	 * 			| new.getX() == this.getX() + (nbSteps * this.getRadius() * Math.cos(this.getDirection()))
	 * 			| new.getY() == this.getY() + (nbSteps * this.getRadius() * Math.sin(this.getDirection()))
	 * @param 	nbSteps
	 * @throws	IllegalArgmentException
	 * 			| nbSteps < 0) || (! this.canMove(nbSteps)) 
	 */
	public void move(int nbSteps) throws IllegalArgumentException {
		if( (nbSteps < 0) || (! this.canMove(nbSteps)) )
			throw new IllegalArgumentException();
		this.setX( this.getX() + (nbSteps * this.getRadius() * Math.cos(this.getDirection())) );
		this.setY( this.getY() + (nbSteps * this.getRadius() * Math.sin(this.getDirection())) );
		this.setActionPoints(this.getActionPoints() - this.calculateMoveCost(nbSteps));
	};
	
	/**
	 * Calculates the number of action points needed for moving this worm a given number of steps in the current direction.
	 * @param nbSteps
	 * @return	Return the number of action points needed for moving this worm a given number of steps in the current direction.
	 * 			| result == (int) Math.ceil( nbSteps * ( Math.abs(Math.cos(this.getDirection())) + Math.abs(4 * Math.sin(this.getDirection())) ) );
	 */
	public int calculateMoveCost(int nbSteps) {
		return (int) Math.ceil( nbSteps * ( Math.abs(Math.cos(this.getDirection())) + Math.abs(4 * Math.sin(this.getDirection())) ) );
	}

	/**
	 * Returns whether or not the given worm can turn by the given angle.
	 * @pre		The angle over which the worm is turned lies between -PI and PI.
	 * 			| -Math.PI <= angle && angle < Math.PI
	 * @param	angle
	 * @return	Returns whether or not the given worm can turn by the given angle.
	 * 			| result == this.calculateTurnCost(angle) <= this.getActionPoints();
	 */
	public boolean canTurn(double angle) {
		return Util.fuzzyLessThanOrEqualTo( this.calculateTurnCost(angle), this.getActionPoints() );
	};
	
	/**
	 * Checks if the given angle is a valid angle over which this worm can turn.
	 * @param angle
	 * @return	Returns whether the angle over which the worm is turned is a number and not infinite.
	 * 			| result == !Double.isNaN(angle) && !Double.isInfinite(angle)
	 */
	public static boolean isValidTurnAngle(double angle) {
		return !Double.isNaN(angle) && !Double.isInfinite(angle);
	}

	/**
	 * Turns the given worm by the given angle.
	 * @pre		The angle must be valid.
	 * 			| this.isValidTurnAngle(angle)
	 * @pre		This worm must be able to turn.
	 * 			| this.canTurn(angle);
	 * @post	Current direction changed with the given direction.
	 * 			| new.getDirection() == this.setDirection( restrictAngle( this.getDirection() + angle ) )
	 * @param	angle	
	 */
	public void turn(double angle) {
		assert this.isValidTurnAngle(angle);
		assert this.canTurn(angle);
		this.setDirection( restrictDirection( this.getDirection() + angle ) );
		this.setActionPoints(this.getActionPoints() - this.calculateTurnCost(angle));
	};
	
	/**
	 * Calculates the number of action points needed for turning a worm over a given angle.
	 * @param angle
	 * @return	Return the number of action points needed for turning a worm over a given angle.
	 * 			|  result == (int) Math.ceil( 60 * Math.abs(angle) / (2*Math.PI) )
	 */
	public int calculateTurnCost(double angle) {
		return (int) Math.ceil( 60 * Math.abs(angle) / (2*Math.PI) );
	}
	
	/**
	 * Converts the given angle into an angle between 0 (included) and 2 PI (not included)
	 * @param angle
	 * @return	Return the given angle converted into an angle between 0 (included) and 2 PI (not included)
	 * 			| while(angle < 0) { angle += 2 * Math.PI; } 
	 * 			| while(angle >= 2 * Math.PI) { angle -= 2 * Math.PI; }
	 * 			| result == angle;
	 */
	public static double restrictDirection(double angle) {
		while (Util.fuzzyLessThanOrEqualTo(angle, 0.0)) {
			angle += 2 * Math.PI;
		}
		while (Util.fuzzyGreaterThanOrEqualTo(angle, 2*Math.PI)) {
			angle -= 2 * Math.PI;
		}
		return angle;
	}

	/**
	 * Makes the given worm jump.
	 * @post	The x value of the worm is increased by the distance of the jump.
	 * 			| new.getX() == this.getX() + distance 
	 * @post	The action points are set to 0.
	 * 			| new.getActionPoints() == 0
	 * @throws	ArithmeticException
	 * 			If this worm is trying to jump while facing straight up.
	 * 			| if( Util.fuzzyEquals(this.getDirection(), Math.PI/2) )
	 * @throws	UnsupportedOperationException
	 * 			If this worm doesn't have any action points anymore.
	 * 			| if( this.getActionPoints() == 0 )
	 * @throws 	UnsupportedOperationException
	 * 			If this worm wants to jump while facing down.
	 * 			| (Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)
	 */
	public void jump() throws ArithmeticException, UnsupportedOperationException {
		if( Util.fuzzyEquals(this.getDirection(), Math.PI/2) ) 
			throw new ArithmeticException();
		if(this.getActionPoints() == 0 )
			throw new UnsupportedOperationException();
		if(Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)) 
			throw new UnsupportedOperationException();
		double force = (5 * this.getActionPoints()) + this.getMass() * gravity;
		double v0 = (force/this.getMass()) * 0.5;
		double distance = (Math.pow(v0, 2) * Math.sin(2 * this.getDirection()) / gravity);
		this.setX( this.getX() + distance );
		this.setActionPoints(0);
	};

	/**
	 * Returns the total amount of time (in seconds) that a jump of the given worm would take.
	 * @return	Return the total amount of time (in seconds) that a jump of the given worm would take.
	 * 			| result == distance/(v0 * Math.cos(this.getDirection()))
	 * @throws	ArithmeticException
	 * 			If this worm is trying to jump while facing straight up.
	 * 			| if( Util.fuzzyEquals(this.getDirection(), Math.PI/2) )
	 * @throws	UnsupportedOperationException
	 * 			If this worm doesn't have any action points anymore.
	 * 			| if( this.getActionPoints() == 0 )
	 * @throws 	UnsupportedOperationException
	 * 			If this worm wants to jump while facing down.
	 * 			| (Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)
	 */
	public double getJumpTime() throws ArithmeticException, UnsupportedOperationException {
		if( Util.fuzzyEquals(this.getDirection(), Math.PI/2) ) 
			throw new ArithmeticException();
		if(this.getActionPoints() == 0 )
			throw new UnsupportedOperationException();
		if(Util.fuzzyGreaterThanOrEqualTo(this.getDirection(), Math.PI)) 
			throw new UnsupportedOperationException();
		double force = (5 * this.getActionPoints()) + this.getMass() * gravity;
		double v0 = (force/this.getMass()) * 0.5;
		double distance = (Math.pow(v0, 2) * Math.sin(2 * this.getDirection()) / gravity);
		return distance/(v0 * Math.cos(this.getDirection()));
	};

	/**
	 * Returns the location on the jump trajectory of the given worm
	 * after a time t.
	 * @param t
	 * @return 	An array with two elements, with the first element being the x-coordinate and the second element the y-coordinate.
	 * 			| double x = this.getX() + v0 * Math.cos(this.getDirection()) * t;
	 * 			| double y = this.getY() + v0 * Math.sin(this.getDirection()) * t - 0.5 * gravity * Math.pow(t,2);
	 * 			| result == {x,y}
	 * @throws	IllegalArgumentException
	 *  		| Double.isNaN(t)
	 * @throws	IllegalArgumentException
	 * 			| (t < 0) || (t > this.getJumpTime)
	 */
	public double[] getJumpStep(double t) throws IllegalArgumentException {
		if(Double.isNaN(t)) 
			throw new IllegalArgumentException();
		if((t < 0) || (t > this.getJumpTime())) 
			throw new IllegalArgumentException();
		double force = (5 * this.getActionPoints()) + this.getMass() * gravity;
		double v0 = (force/this.getMass()) * 0.5;
		double x = this.getX() + v0 * Math.cos(this.getDirection()) * t;
		double y = this.getY() + v0 * Math.sin(this.getDirection()) * t - 0.5 * gravity * Math.pow(t,2);
		double[] result = {x,y}; 
		return result;	
	};
	
	private final double gravity = 9.80665;
	
}
