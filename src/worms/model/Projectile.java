package worms.model;

import worms.util.Util;


/**
 * @invar	The yield of this projectile is always valid, as specified in isValidYield().
 * 			| isValidYield(this.getYield())
 * @author Nick
 *
 */
public abstract class Projectile extends Jumpable {
	
	public Projectile(double x, double y, double radius) {
		super(x, y, radius);
	}
	
	/**
	 * Sets the yield of the projectile to the given yield.
	 * @param 	yield 
	 * @throws 	IllegalArgumentException
	 * 			| !isValid(yield)
	 */
	public void setYield(double yield) throws IllegalArgumentException {
		if(!isValidYield(yield))
			throw new IllegalArgumentException();
		this.yield = yield;
	}
	
	/**
	 * Returns the yield of this Projectile.
	 * @return	Returns the yield of this Projectile.
	 * 			| result = this.yield
	 */
	protected double getYield() {
		return this.yield;
	}
	
	private double yield;
	
	/**
	 * Checks if the given yield is a number that lies between 0 and 100 (both included).
	 * @param yield
	 * @return Checks if the given yield is a number that lies between 0 and 100 (both included).
	 * 			| (!Double.isNaN(yield)) &&
				| (Util.fuzzyGreaterThanOrEqualTo(yield, 0)) &&
				| (Util.fuzzyLessThanOrEqualTo(yield, 100))
	 */
	public static boolean isValidYield(double yield) {
		return (!Double.isNaN(yield)) &&
				(Util.fuzzyGreaterThanOrEqualTo(yield, 0)) &&
				(Util.fuzzyLessThanOrEqualTo(yield, 100));
	}
	
	public abstract double calculateForce();
	
	protected static double calculateRadius(double mass) {
		return Math.pow((mass/(density*(4.0/3.0)*Math.PI)), (1.0/3.0));
	}
	
	private static final double density = 7800;
	
	@Override
	public void jump(double timeStep) throws UnsupportedOperationException {
		try {
			double force = this.calculateForce();
			super.jump(timeStep, force);
		} catch(UnsupportedOperationException uOExc) {
			throw uOExc;
		}
	}
	
	/**
	 * Returns the location on the jump trajectory of the given projectile after a
	 * time t.
	 * 
	 * @return An array with two elements, with the first element being the
	 *         x-coordinate and the second element the y-coordinate
	 */
	public double[] getJumpStep(Projectile projectile, double t) {
		
	};
	
	/**
	 * Determine the time that the given projectile can jump until it hits the terrain, hits a worm, or leaves the world.
	 * The time should be determined using the given elementary time interval.
	 * 
	 * @param projectile The projectile for which to calculate the jump time.
	 * 
	 * @param timeStep An elementary time interval during which you may assume
	 *                 that the projectile will not completely move through a piece of impassable terrain.
	 *                 
	 * @return The time duration of the projectile's jump.
	 */
	public double getJumpTime(Projectile projectile, double timeStep) {
		
	};
	
	/**
	 * Returns whether the given projectile is still alive (active).
	 */
	public boolean isActive(Projectile projectile) {
		
	};
	
	/**
	 * Returns the active projectile in the world, or null if no active projectile exists.
	 */
	public Projectile getActiveProjectile(World world) {
		
	};
}
