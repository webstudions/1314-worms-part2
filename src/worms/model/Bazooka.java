package worms.model;

public class Bazooka extends Projectile {
	
	public Bazooka(double x, double y, double radius) {
		super(x, y, Projectile.calculateRadius(mass));
	}

	@Override
	public double calculateForce() {
		return (this.getYield()/100) * 7 + 2.5;
	}

	@Override
	protected void setMass() {}
	
	private static final double mass = 0.3;
	
}
