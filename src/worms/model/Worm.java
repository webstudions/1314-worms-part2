package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

public class Worm extends Jumpable {
	
	/**
	 * Create a new worm that is positioned at the given location in the given world,
	 * looks in the given direction, has the given radius and the given name.
	 * 
	 * @param world
	 * The world in which to place the created worm  
	 * @param x
	 * The x-coordinate of the position of the new worm (in meter)
	 * @param y
	 * The y-coordinate of the position of the new worm (in meter)
	 * @param direction
	 * The direction of the new worm (in radians)
	 * @param radius 
	 * The radius of the new worm (in meter)
	 * @param name
	 * The name of the new worm
	 */
	Worm (World world, double x, double y, double direction,
			double radius, String name) {
		
	};
	
	/**
	 * Sets the mass of this worm to the given mass.
	 * @post	Sets the mass of this worm using the radius of this worm, expressed in kilograms.
	 * 			| new.getMass() == density * ( 4/3 * Math.PI * Math.pow(this.getRadius(), 3) );
	 */
	@Raw @Override
	protected void setMass() {
		this.mass = density * ( (4.0/3.0) * Math.PI * Math.pow(this.getRadius(), 3) );
	}
	
	private final double density = 1062;

	/**
	 * Return the maximum action points this worm can have.
	 */
	@Basic
	public int getMaximumActionPoints() {
		return this.maximumActionPoints;
	}
	
	/**
	 * Sets the maximum action points this worm can have based on its mass.
	 * @post	Sets the maximum action points of this worm using its mass. If the mass leads to a too large integer, max action points are set to max integer.
	 * 			| long newMAP = Math.round( this.getMass() );
				| if (newMAP >= Integer.MAX_VALUE) {
				| 	this.maximumActionPoints = Integer.MAX_VALUE;
				| } else {
				|	this.maximumActionPoints = (int) newMAP; 
				| }
	 */
	@Raw
	private void setMaximumActionPoints() {
		long newMAP = Math.round( this.getMass() );
		if (newMAP >= Integer.MAX_VALUE) {
			this.maximumActionPoints = Integer.MAX_VALUE;
		} else {
			this.maximumActionPoints = (int) newMAP; 
		}
	}

	private int maximumActionPoints;
	
	/**
	 * Return the number of action points of this worm.
	 */
	@Basic
	public int getActionPoints() {
		return actionPoints;
	}
	
	/**
	 * Set the action points of this worm to the given number of action points.
	 * @post	Set the action points of this worm to the given number of action points.
	 * 			| if(actionPoints < 0) { 
	 * 			|		new.getActionPoints() == actionPoints;
	 * 			|	}
	 * 			| else if( (actionPoints >= 0) && (actionPoints<=this.getMaximumActionPoints() ) 
	 * 			|	{
	 * 			|		this.actionPoints = actionPoints;
	 * 			|	}
	 * 			| else {
	 * 			|		this.actionPoints = this.getMaximumActionPoints();	
	 * 			|	}
	 * @param actionPoints
	 */
	@Raw
	public void setActionPoints(int actionPoints) {
		if(actionPoints < 0) {
			this.actionPoints = 0;
		} else if( (actionPoints>=0) && (actionPoints<=this.getMaximumActionPoints()) ) {
			this.actionPoints = actionPoints;
		} else { // bigger than the maximum
			this.actionPoints = this.getMaximumActionPoints();
		}
	}
	
	private int actionPoints;
	
	/**
	 * Makes the given worm fall down until it rests on impassable terrain again.
	 */
	void fall(Worm worm) {
		
	};
	
	/**
	 * Returns whether or not the given worm can fall down
	 */
	public boolean canFall(Worm worm) {
		
	};

	/**
	 * Returns whether or not the given worm is allowed to move.
	 */
	public boolean canMove(Worm worm) {
		
	};

	/**
	 * Returns whether or not the given worm can turn by the given angle.
	 */
	public boolean canTurn(Worm worm, double angle) {
		
	};
	
	/**
	 * Returns the current number of hit points of the given worm.
	 */
	public int getHitPoints(Worm worm) {
		
	};
	
	/**
	 * Make the given worm jump to its new location. The new location should be determined using the given elementary time interval. 
	 * @param timeStep An elementary time interval during which you may assume
	 *        that the worm will not completely move through a piece of impassable terrain.
	 * @throws UnsupportedOperationException 
	 */
	@Override
	public void jump(double timeStep) throws UnsupportedOperationException {
		if(this.getActionPoints() == 0 )
			throw new UnsupportedOperationException();
		try {
			double force = (5 * this.getActionPoints()) + this.getMass() * gravity;
			super.jump(timeStep, force);
			this.setActionPoints(0);
			if(this.canEat()) {
				this.eat();
			}
			if(this.isOutsideWorld()) {
				this.kill();
			}
		} catch(UnsupportedOperationException uOExc) {
			throw uOExc;
		}
	};
	
	/**
	 * Returns the location on the jump trajectory of the given worm after a
	 * time t.
	 * 
	 * @return An array with two elements, with the first element being the
	 *         x-coordinate and the second element the y-coordinate
	 */
	public double[] getJumpStep(Worm worm, double t) {
		
	};
	
	/**
	 * Determine the time that the given worm can jump until it hits the terrain or leaves the world.
	 * The time should be determined using the given elementary time interval.
	 * 
	 * @param worm The worm for which to calculate the jump time.
	 * 
	 * @param timeStep An elementary time interval during which you may assume
	 *                 that the worm will not completely move through a piece of impassable terrain.
	 *                 
	 * @return The time duration of the worm's jump.
	 */
	public double getJumpTime(Worm worm, double timeStep){
		
	};

	/**
	 * Returns the mass of the given worm.
	 */
	public double getMass(Worm worm) {
		
	};

	/**
	 * Returns the maximum number of action points of the given worm.
	 */
	public int getMaxActionPoints(Worm worm) {
		
	};

	/**
	 * Returns the maximum number of hit points of the given worm.
	 */
	public int getMaxHitPoints(Worm worm) {
		
	};

	/**
	 * Returns the minimal radius of the given worm.
	 */
	public double getMinimalRadius(Worm worm) {
		
	};

	/**
	 * Returns the name the given worm.
	 */
	public String getName(Worm worm) {
		
	};

	/**
	 * Returns the current orientation of the given worm (in radians).
	 */
	public double getOrientation(Worm worm) {
		
	};
	
	/**
	 * Returns the radius of the given worm.
	 */
	public double getRadius(Worm worm) {
		
	};
	
	/**
	 * Returns the name of the weapon that is currently active for the given worm,
	 * or null if no weapon is active.
	 */
	public String getSelectedWeapon(Worm worm) {
		
	};

	/**
	 * Returns the name of the team of the given worm, or returns null if this
	 * worm is not part of a team.
	 * 
	 * (For single-student groups that do not implement teams, this method should always return null)
	 */
	public String getTeamName(Worm worm) {
		
	};
	
	/**
	 * Returns the x-coordinate of the current location of the given worm.
	 */
	public double getX(Worm worm) {
		
	};
	
	/**
	 * Returns the y-coordinate of the current location of the given worm.
	 */
	public double getY(Worm worm) {
		
	};
	
	/**
	 * Returns whether the given worm is alive
	 */
	public boolean isAlive(Worm worm) {
		
	};

	/**
	 * Moves the given worm according to the rules in the assignment.
	 */
	public void move(Worm worm) {
		
	};

	/**
	 * Renames the given worm.
	 */
	public void rename(Worm worm, String newName) {
		
	};

	/**
	 * Activates the next weapon for the given worm
	 */
	public void selectNextWeapon(Worm worm) {
		
	};

	/**
	 * Sets the radius of the given worm to the given value.
	 */
	public void setRadius(Worm worm, double newRadius) {
		
	};

	/**
	 * Makes the given worm shoot its active weapon with the given propulsion yield.
	 */
	public void shoot(Worm worm, int yield) {
		
	};
	
	/**
	 * Turns the given worm by the given angle.
	 */
	public void turn(Worm worm, double angle) {
		
	};

}
