package worms.model;

import be.kuleuven.cs.som.annotate.*;

@Value
public class Position implements Cloneable {
	
	/**
	 * Initializing this position with given x and y coordinate.
	 * @param 	x
	 * 		  	The x coordinate for this worm in meters.
	 * @param 	y
	 * 		  	The y coordinate for this worm in meters.
	 * @throws 	| IllegalArgumentException
	 * 			| 	Double.isNaN(x) || Double.isNaN(y)
	 */
	public Position(Double x, Double y) 
		throws IllegalArgumentException
	{
		if(Double.isNaN(x) || Double.isNaN(y))
			throw new IllegalArgumentException();
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @return	Return the x coordinate of this position.
	 * 			| result == this.x
	 */
	public Double getX() {
		return this.x;
	}
	
	private final Double x;
	
	/**
	 * @return	Return the y coordinate of this position.
	 * 			| result == this.y
	 */
	public Double getY() {
		return this.y;
	}
	
	private final Double y;
	
	/**
	 * Returns a Position that is identical to this identical.
	 * @return	new Position(this.getX(), this.getY());
	 */
	public Position getCopy() {
		return new Position(this.getX(), this.getY());
	}
}
