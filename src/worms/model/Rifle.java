package worms.model;

public class Rifle extends Projectile {

	public Rifle(double x, double y, double radius) {
		super(x, y, Projectile.calculateRadius(mass));
	}

	@Override
	public double calculateForce() {
		return 1.5;
	}

	@Override
	protected void setMass() {}
	
	private static final double mass = 0.01;

}
